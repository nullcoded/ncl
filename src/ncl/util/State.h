#ifndef NULLCODED_UTIL_STATE_H
#define NULLCODED_UTIL_STATE_H

#include <map>
#include <stdint.h>
#include <string>

#include "ncl/logging/logging.h"
#include "ncl/util/util.h"

namespace util {

template <typename state_t> class State {
    struct Members {
        state_t state;
        uint64_t ts;
        logging::log_level_t verbosity;
    } m;
    const std::map<state_t, std::string> *labels;

    inline State(Members &&m_) : m(m_)
    {
    }

   public:
    ~State()
    {
        m = Members{};
    }
    State(const state_t e, const std::map<state_t, std::string> &l)
        : m(Members{ .state = e }), labels(&l)
    {
    }
    inline state_t get()
    {
        return m.state;
    }
    inline void set(state_t e)
    {
        if (m.verbosity > logging::log_level_t::silence) {
            LOG("before -> %s, %llu", as_string().c_str(), elapsed_time_us());
        }
        m.state = e;
        m.ts = get_time_us();
        if (m.verbosity > logging::log_level_t::silence) {
            LOG("now -> %s, %llu", as_string().c_str(), elapsed_time_us());
        }
    }
    inline uint64_t elapsed_time_us()
    {
        return get_time_us() - m.ts;
    }
    inline bool has_time_elapsed_us(uint64_t time_us)
    {
        return elapsed_time_us() > time_us;
    }
    State<state_t> &operator=(state_t new_state)
    {
        set(new_state);
        return *this;
    }
    State<state_t> &operator=(State<state_t> *pNewState)
    {
        set(pNewState->get());
        return *this;
    }
    State<state_t> &operator=(State<state_t> newState)
    {
        set(newState.get());
        return *this;
    }
    bool operator==(const state_t new_state)
    {
        return get() == new_state;
    }
    bool operator==(const State<state_t> &newState)
    {
        return get() == newState.get();
    }
    std::string as_string()
    {
        return labels->at(m.state);
    }
    void enable_logging(bool enable)
    {
        m.verbosity = enable ? logging::log_level_t::info : logging::log_level_t::silence;
    }
    void print()
    {
        LOG("%s, %llu", as_string().c_str(), elapsed_time_us());
    }

};  // struct State

}  // namespace util

#endif  // NULLCODED_UTIL_STATE_H
