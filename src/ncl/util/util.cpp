#include "ncl/util/util.h"

#include <cassert>

#include <stdio.h>
#ifdef PICO
//#include "pico/stdlib.h"
#else
#include <unistd.h> 
#endif //PICO

static util::time_fn_t p_get_time_us = nullptr;

size_t util::print_version(char *buf, const size_t buf_size, Version *p_version)
{
    size_t count = 0;
    if(buf)
    {
        count += sprintf(buf, "version:%d.%d.%d-%d\n", p_version->major, p_version->minor, p_version->patch, p_version->build);
    }
    return count;
}

size_t util::print_as_hex(char *buf,
                      const size_t buf_size,
                      const std::uint8_t *const data,
                      const size_t len)
{
    size_t print_len = 0;
    if(buf)
    {
        print_len += sprintf(buf+print_len, "(%zu)[", len);
        for (size_t i = 0; i < len && print_len + 4 < buf_size; ++i) {
            print_len += sprintf(buf+print_len, " %02x", static_cast<unsigned char>(data[i]));
        }
        print_len += sprintf(buf+print_len, "]\n");
    }
    return print_len;
}

size_t util::print_as_string(char *buf,
                         const size_t buf_size,
                         const std::uint8_t *const data,
                         const size_t len)
{
    size_t print_len = 0;
    if(buf)
    {
        print_len += sprintf(buf+print_len, "(%zu)'", len);
        for (size_t i = 0; i < len && print_len + 2 < buf_size; ++i) {
            print_len += sprintf(buf+print_len, "%c", static_cast<unsigned char>(data[i]));
        }
        if (print_len + 2 < buf_size) {
            print_len += sprintf(buf+print_len, "'\n");
        }
    }
    return print_len;
}

uint64_t util::get_time_us()
{
    uint64_t time = 0;
#ifdef PICO
    assert(p_get_time_us!=nullptr);
    time=p_get_time_us();
#else
    struct timespec spec;
    clock_gettime(CLOCK_MONOTONIC, &spec);
    time = spec.tv_sec * util::us_per_s;
    time += spec.tv_nsec / util::ns_per_ms;
#endif //PICO
    return time;
}

void util::init(time_fn_t p_fn)
{
    p_get_time_us=p_fn;
}
