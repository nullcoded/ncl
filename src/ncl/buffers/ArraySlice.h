
#ifndef NULLCODED_BUFFERS_ARRAYSLICE_H
#define NULLCODED_BUFFERS_ARRAYSLICE_H

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <memory>
#include <optional>
#include <stddef.h>

#include "ncl/buffers/defs.h"
#include "ncl/logging/logging.h"
#include "ncl/util/Status.h"
#include "ncl/util/util.h"

namespace buffers {

class Array;
class ArraySlice;
class ArrayBuffer;

/*
 * Array - interface to push/pop or get values from bytes, bytes container implemented in
 * decendants.
 */
class Array {
    Array(Array &) = delete;
    Array(Array &&) = delete;

   public:
    Array(){};  // Nil array with no data.
    // Required to be implemented by decendants
    virtual std::uint8_t *data()
    {
        return nullptr;
    }
    virtual size_t pos()
    {
        return 0;
    }
    virtual size_t len()
    {
        return 0;
    }
    virtual size_t capacity()
    {
        return 0;
    }
    virtual size_t remaining_space()
    {
        return capacity() - len();
    }
    virtual size_t remaining_data()
    {
        return len() - pos();
    }
    virtual void pos(size_t p)
    {
    }
    virtual void len(size_t l)
    {
    }

    // backeimplementation
    virtual std::uint8_t operator[](size_t offset)
    {
        return *data_at(offset);
    }

    virtual std::uint8_t *data_at(size_t offset)
    {
        if (data() == nullptr) {
            return nullptr;
        }

        return data() + offset;
    }

    void print_as_hex()
    {
        const std::size_t print_buffer_len = len() * 4 + 128;
        std::unique_ptr<char[]> _print_buffer(new char[print_buffer_len]);
        char *print_buffer = _print_buffer.get();
        size_t l = util::print_as_hex(print_buffer, print_buffer_len, data(), len());
        if (l > print_buffer_len) {
            LOG_E("%s", "buffer out of bounds");
        } else {
            LOG_I("%s", print_buffer);
        }
    }

    void print_array_str()
    {
        const std::size_t print_buffer_len = len() + 128;
        std::unique_ptr<char[]> _print_buffer(new char[print_buffer_len]);
        char *print_buffer = _print_buffer.get();
        util::print_as_string(print_buffer, print_buffer_len, data(), len());
        LOG_I("%s", print_buffer);
    }

    Status push(const char *const a2_data, const size_t a2_len)
    {
        return push(reinterpret_cast<const std::uint8_t *>(a2_data), a2_len);
    }
    Status push(const std::uint8_t *const a2_data, const size_t a2_len)
    {
        Status status(status_t::success, status_labels);
        //LOG_I("push %u bytes", a2_len);
        if (NULL == a2_data) {
            status = err_nullptr_not_allowed;
        }
        if (0 == a2_len) {
            status = err_empty_input;
        }
        if (a2_len + len() > capacity()) {
            status = err_capacity_exceeded;
        }
        if (status.is_success()) {
            std::copy(a2_data, (a2_data + a2_len), (data() + len()));
            len(len() + a2_len);
        }
        return status;
    }

    Status push(Array &a2)
    {
        return push(a2.data(), a2.len());
    }

    Status pop(size_t pop_len,
               std::uint8_t *const dest_data,
               const size_t dest_capacity,
               const size_t dest_offset)
    {
        Status status(status_t::success, status_labels);
        if (NULL == dest_data) {
            status = err_nullptr_not_allowed;
        }
        if (dest_capacity == 0) {
            status = err_empty_output;
        }
        if (pop_len + dest_offset > dest_capacity) {
            status = err_output_capacity_exceeded;
        }
        if (pop_len > len()) {
            status = err_length_exceeded;
        }
        if (status.is_success()) {
            std::copy(data(), (data() + pop_len), (dest_data + dest_offset));
            len(len() - pop_len);
            std::copy((data() + pop_len), (data() + pop_len + len()), data());
        }
        return status;
    }

    Status pop(size_t pop_len, Array &dest)
    {
        Status status = pop(pop_len, dest.data(), dest.capacity(), dest.len());
        if (status.is_success()) {
            dest.len(dest.len() + pop_len);
        }
        return status;
    }
    Status drain(size_t drain_len)
    {
        Status status(status_t::success, status_labels);
        if (status.is_success()) {
            std::copy((data() + drain_len), (data() + drain_len + len()), data());
            len(len() - drain_len);
        }
        if (pos() < drain_len) {
            pos(0);
        } else {
            pos(pos() - drain_len);
        }
        return status;
    }
    Status fill(uint8_t filler, size_t fill_len)
    {
        Status status(status_t::success, status_labels);
        if (fill_len > capacity() - len()) {
            status = err_length_exceeded;
        }
        if (status.is_success()) {
            for (size_t i = 0; i < fill_len && status.is_success(); ++i) {
                status = push_type(filler);
            }
        }
        return status;
    }
    Status clear()
    {
        Status status(status_t::success, status_labels);
        len(0);
        pos(0);
        return status;
    }

    /**
     * Push uintX_t
     */
    template <typename T> Status push_type(T val)
    {
        return push(reinterpret_cast<std::uint8_t *>(&val), sizeof(T));
    }

    template <typename T> Status pop_type(T &val)
    {
        return pop(sizeof(T), reinterpret_cast<std::uint8_t *>(&val), sizeof(T), 0);
    }
    /**
     * Get - return range of bytes and move pos
     */
    Status get(size_t get_len,
               char *const dest_data,
               const size_t dest_capacity,
               const size_t dest_offset)
    {
        return get(get_len, reinterpret_cast<std::uint8_t *>(dest_data), dest_capacity, dest_offset);
    }
    Status get(size_t get_len,
               std::uint8_t *const dest_data,
               const size_t dest_capacity,
               const size_t dest_offset)
    {
        Status status(status_t::success, status_labels);
        if (NULL == dest_data) {
            status = err_nullptr_not_allowed;
        }
        if (dest_capacity == 0) {
            status = err_empty_output;
        }
        if (get_len + dest_offset > dest_capacity) {
            status = err_output_capacity_exceeded;
        }
        if (get_len > len()) {
            status = err_length_exceeded;
        }
        if (status.is_success()) {
            std::copy_n(data_at(pos()), get_len, (dest_data + dest_offset));
            pos(pos() + get_len);
        }
        return status;
    }

    Status get(size_t get_len, Array &dest)
    {
        Status status = get(get_len, dest.data(), dest.capacity(), dest.len());
        if (status.is_success()) {
            dest.len(dest.len() + get_len);
        }
        return status;
    }
    /**
     * Get type, move pos
     */
    template <typename T> Status get_type(T &val)
    {
        return get(sizeof(T), reinterpret_cast<std::uint8_t *>(&val), sizeof(T), 0);
    }

    /**
     * Get uintX_t from position i
     */
    template <typename T> std::optional<T> get_uint_at(size_t i)
    {
        if (i + sizeof(T) > len()) {
            return {};
        } else {
            return util::get_uint_from_bytes<T>((data_at(i)), len() - i);
        }
    }

    /**
     * probe -  retrieve uintX_t from internal bytes array at pos and move pos forward
     */
    template <typename T> std::optional<T> probe_uint()
    {
        if (pos() + sizeof(T) > len()) {
            return {};
        }
        return util::get_uint_from_bytes<T>((data_at(pos())), len() - pos());
    }
    /**
     * Get uintX_t from position i in host byte order from network order(bigendian)
     */
    template <typename T> std::optional<T> net_get_uint_at(size_t i)
    {
        auto opt = get_uint_at<T>(i);
        if (!opt.has_value()) {
            return opt;
        }
        return util::htobe<T>(opt.value());
    }

    /**
     * probe -  retrieve uintX_t from internal bytes array at pos in host byte order from network
     * order(bigendian)
     */
    template <typename T> std::optional<T> net_probe_uint()
    {
        auto opt = probe_uint<T>();
        if (!opt.has_value()) {
            return opt;
        }
        return util::htobe<T>(opt.value());
    }
};

class ArraySlice : public Array {
    struct Members {
        std::uint8_t *const pData;
        size_t pos;
        size_t len;
    } m;
    const size_t cap;

   public:
    ArraySlice(std::uint8_t *const p, size_t c) : m(Members{ .pData = p}), cap(c)
    {
    }
    ArraySlice(std::uint8_t *const p, size_t l, size_t c) : m(Members{ .pData = p, .len = l }), cap(c)
    {
    }
    ArraySlice(Array &buf)
        : m(Members{ .pData = buf.data(), .pos = 0, .len = buf.len() }), cap(buf.capacity())
    {
    }
    ArraySlice(Array &&buf)
        : m(Members{ .pData = buf.data(), .pos = 0, .len = buf.len() }), cap(buf.capacity())
    {
    }
    ArraySlice(ArraySlice &buf)
        : m(Members{ .pData = buf.data(), .pos = buf.pos(), .len = buf.len() }), cap(buf.capacity())
    {
    }
    ArraySlice(ArraySlice &&buf)
        : m(Members{ .pData = buf.data(), .pos = buf.pos(), .len = buf.len() }), cap(buf.capacity())
    {
    }

    virtual std::uint8_t *data()
    {
        return m.pData;
    }
    virtual size_t pos()
    {
        return m.pos;
    }
    virtual size_t len()
    {
        return m.len;
    }
    virtual size_t capacity()
    {
        return cap;
    }
    virtual void pos(size_t p)
    {
        m.pos = p;
    }
    virtual void len(size_t l)
    {
        m.len = l;
    }
};

class ArrayBuffer : public Array {
    struct Members {
        size_t pos;
        size_t len;
    } m;
    std::unique_ptr<std::uint8_t[]> data_;
    const size_t cap;

   public:
    virtual std::uint8_t *data()
    {
        return data_.get();
    }

    virtual size_t pos()
    {
        return m.pos;
    }
    virtual size_t len()
    {
        return m.len;
    }
    virtual size_t capacity()
    {
        return cap;
    }
    virtual void pos(size_t p)
    {
        m.pos = p;
    }
    virtual void len(size_t l)
    {
        m.len = l;
    }


    ArrayBuffer(size_t c, std::uint8_t *p) : m(Members{.pos=0, .len=c}), cap(c)
    {
        data_.reset(new std::uint8_t[c]);
        std::copy_n(p, c, data_.get());
    }

    ArrayBuffer(size_t c) : m(Members{}), cap(c)
    {
        data_.reset(new std::uint8_t[c]);
    }

    ArrayBuffer(ArrayBuffer &buf) : m(buf.m), cap(buf.cap)
    {
        data_.reset(new std::uint8_t[cap]);
        std::copy(buf.data(), (buf.data() + buf.len()), data());
    }

    ~ArrayBuffer()
    {
        //data will free with unique_ptr
    }

    /**
     * slice - return non-owning subsection of the buffer
     */
    std::optional<ArraySlice> slice(size_t offset, const size_t slice_len, const size_t slice_cap)
    {
        if (slice_len > len()) {
            LOG_E("%s", status_labels.at(err_capacity_exceeded).c_str());
            return std::nullopt;
        }
        if (slice_cap > capacity()) {
            LOG_I("%s", status_labels.at(err_length_exceeded).c_str());
            return std::nullopt;
        }
        return std::optional<ArraySlice>{ ArraySlice(data_at(offset), slice_len, slice_len) };
    }

    std::optional<ArraySlice> slice(size_t offset, const size_t slice_len)
    {
        return slice(offset, slice_len, capacity());
    }
    std::optional<ArraySlice> slice()
    {
        return slice(0, len(), capacity());
    }
};  // class ArraySlice

};  // namespace buffers

#endif  // NULLCODED_BUFFERS_ARRAYSLICE_H
