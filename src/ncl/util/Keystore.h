#ifndef NCL_UTIL_KEYSTORE
#define NCL_UTIL_KEYSTORE

#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <map>
#include <string>
#include <unistd.h>

#include "ncl/buffers/ArraySlice.h"
#include "ncl/util/Crc32.h"
#include "ncl/util/Status.h"
#include "ncl/util/types.h"

namespace util {
namespace keystore {

enum status_t {
    okay_ = -127,
    success = 0,
    err_genericFail,
    err_key_is_null,
    err_value_is_null,
    err_key_max_len_exceeded,
    err_value_max_len_exceeded,
    err_not_implemented,
    err_storage_init_failed,
    err_storage_read_failed,
    err_storage_write_failed,
    err_parsing_failed,
    err_serialize_data,
    err_key_is_missing,
    err_type_mismatch,
    err_storage_deinit_failed,
    err_storage_erase_failed,
    err_not_initialized_fail,
};  // enum status_t

const std::map<status_t, std::string> status_labels = {
    { success, "Buffer success" },
    { err_genericFail, "Keystore generic error" },
    { err_key_is_null, "Keystore key is null" },
    { err_value_is_null, "Keystore value is null" },
    { err_key_max_len_exceeded, "Keystore key max len is exceeded" },
    { err_value_max_len_exceeded, "Keystore value max len is exceeded" },
    { err_not_implemented, "Keystore feature implementation missing" },
    { err_storage_init_failed, "Keystore storage failed to initialize" },
    { err_storage_read_failed, "Keystore storage failed to read" },
    { err_storage_write_failed, "Keystore storage failed to write" },
    { err_parsing_failed, "Keystore parsing failed" },
    { err_serialize_data, "Keystore serializing data failed" },
    { err_key_is_missing, "Keystore key is not available" },
    { err_type_mismatch, "Keystore type is not a match" },
    { err_storage_deinit_failed, "Keystore storage failed to deinit" },
    { err_storage_erase_failed, "Keystore storage failed to erase" },
    { err_not_initialized_fail, "Keystore storage was not initialized" },
};  // map<> status_labels

using Status = util::Status<status_t>;

const std::size_t max_buff_len = 128;

const char file_id_1_str[] = "nullcoded";
const char file_id_2_str[] = "keystore";
const std::uint64_t file_id_project_hash = 0x05648b4b;
const std::uint64_t file_id_type_hash = 0x2475f3a5;
const std::uint64_t file_id_hash = (file_id_project_hash << 32) | (file_id_type_hash);
const std::size_t options_size = max_buff_len;
const std::size_t key_size = max_buff_len / 2;
const std::size_t value_size = max_buff_len;
const std::size_t entry_size = key_size + value_size;
using hash_t = std::uint32_t;
using offset_t = std::uint32_t;
using binlen_t = std::uint8_t;
using bintype_t = std::uint8_t;

using hash_fn_t = std::function<hash_t(char *p, std::size_t len)>;
const std::size_t key_str_len = key_size - (sizeof(hash_t) + sizeof(offset_t) + sizeof(binlen_t));
const std::size_t value_bin_len = value_size - (sizeof(binlen_t) + sizeof(util::types::arg_t));

template <typename Type, std::size_t MAX_LEN> struct Bin {
    binlen_t len;
    Type value[MAX_LEN];
};  // struct Bin

using Key = Bin<char, key_str_len>;
struct Value {
    bintype_t type;
    Bin<std::uint8_t, value_bin_len> bin;
};

struct Entry {
    // key info
    hash_t hash;
    offset_t position;
    Key key;
    // value info
    Value value;
};  // struct Entry

std::size_t print_key(Key &k, char *buf, std::size_t len)
{
    std::size_t pos = 0;
    pos += sprintf(buf + pos, "len:%d, key:", k.len);
    std::copy_n(k.value, k.len, buf + pos);
    pos += k.len;
    pos += sprintf(buf + pos, ", ");
    return pos;
}

std::size_t print_value(Value &v, char *buf, std::size_t len)
{
    std::size_t pos = 0;
    pos += sprintf(buf + pos, "type:%d,", v.type);
    pos += sprintf(buf + pos, "len:%d, val:", v.bin.len);
    pos += util::print_as_hex(buf + pos, len, v.bin.value, v.bin.len);
    pos += sprintf(buf + pos, " ");
    return pos;
}

void print_entry(Entry &e)
{
    const std::size_t buf_len = 128;
    char buf[buf_len];
    std::size_t pos = 0;
    pos += sprintf(buf + pos, "hash:%lu, ", e.hash);
    pos += sprintf(buf + pos, "position:%lu, ", e.position);
    pos += sprintf(buf + pos, "\nkey:");
    pos += print_key(e.key, buf + pos, buf_len - pos);
    pos += sprintf(buf + pos, "\nvalue:");
    pos += print_value(e.value, buf + pos, buf_len - pos);
    assert(buf_len > pos);
    LOG_D("\nentry: %s", buf);
}

void log_as_hex(const std::uint8_t *const data, const size_t len)
{
    const std::size_t buf_len = 128;
    char buf[buf_len];
    util::print_as_hex(buf, buf_len, data, len);
    assert(buf_len > len);
    LOG_D("buf:%s", buf);
    return;
}

struct Options {
    std::uint64_t file_id;
    util::Version version;
    std::uint8_t len;
    std::uint16_t entry_count;
    // leave room for future options
};  // struct Options

class Crc32Hash {
    util::Crc32 crc_gen;

   public:
    Crc32Hash() : crc_gen{}
    {
    }
    hash_t operator()(char *p, std::size_t len)
    {
        return crc_gen.calc(reinterpret_cast<const uint8_t *>(p), len);
    }
};  // class Crc32Hash

class StorageHandler {
   public:
    StorageHandler()
    {
    }

    virtual ~StorageHandler()
    {
    }

    virtual Status init()
    {
        Status status{ status_t::success, status_labels };
        return status;
    }

    virtual Status close()
    {
        Status status{ status_t::success, status_labels };
        return status;
    }

    virtual Status write(uint8_t *p_data, std::size_t len)
    {
        Status status{ status_t::success, status_labels };
        status = err_not_implemented;
        return status;
    }

    virtual util::keystore::Status write(buffers::Array &buf)
    {
        util::keystore::Status status{ util::keystore::status_t::success,
                                       util::keystore::status_labels };
        if (write(buf.data_at(buf.pos()), buf.remaining_data()).is_error()) {
            status = util::keystore::status_t::err_storage_write_failed;
        } else {
            buf.pos(buf.pos() + buf.remaining_data());
        }
        return status;
    }

    virtual Status read(uint8_t *p_data, std::size_t capacity, ssize_t &read_count)
    {
        Status status{ status_t::success, status_labels };
        read_count = 0;
        status = err_not_implemented;
        return status;
    }

    virtual Status read(buffers::Array &buffer)
    {
        Status status{ status_t::success, status_labels };
        ssize_t read_count;
        status = read((buffer.data_at(buffer.len())), buffer.remaining_space(), read_count);
        if (read_count > 0) {
            buffer.len(buffer.len() + read_count);
        }
        return status;
    }

    virtual Status clear()
    {
        Status status{ status_t::success, status_labels };
        status = err_not_implemented;
        return status;
    }

};  // class StorageHandler

template <std::size_t STORAGE_SIZE,
          std::size_t entries_count = ((STORAGE_SIZE - options_size) / entry_size)>
struct Keystore {
    struct Members {
    } m;
    hash_fn_t hash_fn;
    StorageHandler &storage;
    Options options{ .file_id = file_id_hash,
                     .version{ .major = 0, .minor = 0, .patch = 0, .build = 1 },
                     .len = sizeof(Options::entry_count) };
    Entry entries[entries_count];
    // map<hash, entries_index>
    std::map<hash_t, std::size_t> map{};

    Keystore(hash_fn_t h_fn, StorageHandler *p_sh) : m(Members{}), hash_fn(h_fn), storage(*p_sh)
    {
    }

    Status serialize()
    {
        Status status{ status_t::success, status_labels };
        // keys are in blocks of 128bytes, if there is an odd number of entries an empty key
        status = storage.init();
        if (status.is_error()) {
            LOG_E("unable to init storage: %s", status.as_string().c_str());
            return status;
        }
        // needs to be added.
        std::size_t key_section_size = ((key_size * 2) * (options.entry_count / 2));
        //buffers::ArrayBuffer buffer{ max_buff_len };
        std::uint8_t raw_buf[max_buff_len];
        buffers::ArraySlice buffer{ raw_buf, max_buff_len };
        // write header,options
        //LOG_D("\nWrite Header: \n");
        if (buffer.push_type(options.file_id).is_error()) {
            status = status_t::err_serialize_data;
            LOG_E("file_id failed to serialize");
            return status;
        }
        //LOG_D("options.file_id:%ld, %lx", options.file_id, options.file_id);
        if (buffer.push_type(options.version.major).is_error()) {
            status = status_t::err_serialize_data;
            LOG_E("options.version.major failed to serialize");
            return status;
        }
        //LOG_D("options.version.major: %d, %02x", options.version.major, options.version.major);
        if (buffer.push_type(options.version.minor).is_error()) {
            status = status_t::err_serialize_data;
            LOG_E("options.version.minor failed to serialize");
            return status;
        }
        //LOG_D("options.version.minor: %d, %02x", options.version.minor, options.version.minor);
        if (buffer.push_type(options.version.patch).is_error()) {
            status = status_t::err_serialize_data;
            LOG_E("options.version.patch failed to serialize");
            return status;
        }
        //LOG_D("options.version.patch: %d, %02x", options.version.patch, options.version.patch);
        if (buffer.push_type(options.version.build).is_error()) {
            status = status_t::err_serialize_data;
            LOG_E("options.version.build failed to serialize");
            return status;
        }
        //LOG_D("options.version.build: %d, %02x", options.version.build, options.version.build);
        if (buffer.push_type(options.len).is_error()) {
            status = status_t::err_serialize_data;
            LOG_E("options.len failed to serialize");
            return status;
        }
        //LOG_D("options.len: %d, %02x", options.len, options.len);
        if (buffer.push_type(options.entry_count).is_error()) {
            status = status_t::err_serialize_data;
            LOG_E("options.entry_count failed to serialize");
            return status;
        }
        //LOG_D("options.entry_count: %u, %02x", options.entry_count, options.entry_count);
        if (buffer.fill(0, buffer.capacity() - buffer.len()).is_error()) {
            status = status_t::err_serialize_data;
            LOG_E("fill failed to serialize");
            return status;
        }
        //LOG_D("buffer.len: %u", buffer.len());
        status = storage.write(reinterpret_cast<std::uint8_t *>(buffer.data()), buffer.len());
        // write the keys in sorted order, provided by map
        //LOG_D("\nWrite Keys: \n");
        buffer.clear();
        for (const auto &[hash, entry_index] : map) {
            Entry &entry = entries[entry_index];
            //LOG_D("entry index: %d", entry_index);
            entry.position = options_size + key_section_size + (value_size * entry_index);
            if (buffer.push_type(entry.hash).is_error()) {
                status = status_t::err_serialize_data;
                LOG_E("entry.hash failed to serialize");
                return status;
            }
            //LOG_D("entry.hash: %d", entry.hash);
            if (buffer.push_type(entry.position).is_error()) {
                status = status_t::err_serialize_data;
                LOG_E("entry.position failed to serialize");
                return status;
            }
            //LOG_D("entry.position: %d", entry.position);
            if (buffer.push_type(entry.key.len).is_error()) {
                status = status_t::err_serialize_data;
                LOG_E("entry.key.len failed to serialize");
                return status;
            }
            //LOG_D("entry.key.len: %d", entry.key.len);
            if (buffer.push(entry.key.value, entry.key.len).is_error()) {
                status = status_t::err_serialize_data;
                LOG_E("entry.key.value failed to serialize");
                return status;
            }
            if (buffer.fill(0, key_size - (buffer.len() % key_size)).is_error()) {
                status = status_t::err_serialize_data;
                LOG_E("fill failed to serialize");
                return status;
            }
            if (buffer.len() == max_buff_len) {
                status
                  = storage.write(reinterpret_cast<std::uint8_t *>(buffer.data()), buffer.len());
                buffer.clear();
            }
        }
        if (buffer.len() > 0) {
            // if there was an odd number of entries, fill the last gap.
            if (buffer.fill(0, key_size).is_error()) {
                status = status_t::err_serialize_data;
                LOG_E("fill failed to serialize");
                return status;
            }
            status = storage.write(reinterpret_cast<std::uint8_t *>(buffer.data()), buffer.len());
            buffer.clear();
        }
        // write values
        //LOG_D("\nWrite Values: \n");
        for (std::size_t entry_index = 0; entry_index < options.entry_count; ++entry_index) {
            Entry &entry = entries[entry_index];
            //LOG_D("entry index %d, pos: %d", entry_index, buffer.pos());
            if (buffer.push_type(entry.value.type).is_error()) {
                status = status_t::err_serialize_data;
                LOG_E("entry.value.type failed to serialize");
                return status;
            }
            //LOG_D("entry.value.type: %d", entry.value.type);
            if (buffer.push_type(entry.value.bin.len).is_error()) {
                status = status_t::err_serialize_data;
                LOG_E("entry.value.bin.len failed to serialize");
                return status;
            }
            //LOG_D("entry.value.bin.len: %d", entry.value.bin.len);
            if (buffer.push(entry.value.bin.value, entry.value.bin.len).is_error()) {
                status = status_t::err_serialize_data;
                LOG_E("entry.value.bin.value failed to serialize");
                return status;
            }
            //LOG_D("entry.value.bin.value: (%d)", entry.value.bin.len);
            if (buffer.fill(0, value_size - (buffer.len() % value_size)).is_error()) {
                status = status_t::err_serialize_data;
                LOG_E("fill failed to serialize");
                return status;
            }
            status = storage.write(reinterpret_cast<std::uint8_t *>(buffer.data()), buffer.len());
            buffer.clear();
        }
        storage.close();
        return status;
    }

    Status deserialize()
    {
        Status status{ status_t::success, status_labels };
        //buffers::ArrayBuffer buffer{ max_buff_len };
        std::uint8_t raw_buf[max_buff_len];
        buffers::ArraySlice buffer{ raw_buf, max_buff_len };

        status = storage.init();
        // read header, options
        //LOG_D("\nRead Header: \n");
        // TODO:: add max_attempts
        while (status.is_okay() && buffer.len() < max_buff_len) {
            status = storage.read(buffer);
        }
        if (status.is_error()) {
            LOG_E("unable to fill buffer");
            return status;
        }
        //LOG_D("pos:%d", buffer.pos());
        if (buffer.get_type(options.file_id).is_error()) {
            status = status_t::err_parsing_failed;
            LOG_E("unable to parse file_id");
            return status;
        }
        //LOG_D("options.file_id:%ld, pos:%d, %lx", options.file_id, buffer.pos(), options.file_id);
        if (buffer.get_type(options.version.major).is_error()) {
            status = status_t::err_parsing_failed;
            LOG_E("unable to parse version.major");
            return status;
        }
        //LOG_D("options.version.major:%d, pos:%d, %02x", options.version.major, buffer.pos(), options.version.major);

        if (buffer.get_type(options.version.minor).is_error()) {
            status = status_t::err_parsing_failed;
            LOG_E("unable to parse version.minor");
            return status;
        }
        //LOG_D("options.version.minor:%d, pos:%d, %02x", options.version.minor, buffer.pos(), options.version.minor);

        if (buffer.get_type(options.version.patch).is_error()) {
            status = status_t::err_parsing_failed;
            LOG_E("unable to parse version.patch");
            return status;
        }
        //LOG_D("options.version.patch:%d, pos:%d, %02x", options.version.patch, buffer.pos(), options.version.patch);

        if (buffer.get_type(options.version.build).is_error()) {
            status = status_t::err_parsing_failed;
            LOG_E("unable to parse version.build");
            return status;
        }
        //LOG_D("options.version.build:%d, pos:%d, %02x", options.version.build, buffer.pos(), options.version.build);

        if (buffer.get_type(options.len).is_error()) {
            status = status_t::err_parsing_failed;
            LOG_E("unable to parse len");
            return status;
        }
        //LOG_D("options.len:%d", options.len);

        if (options.len >= sizeof(options.entry_count)) {
            if (buffer.get_type(options.entry_count).is_error()) {
                status = status_t::err_parsing_failed;
                LOG_E("unable to parse entry_count");
                return status;
            }
            //LOG_D("options.entry_count:%d", options.entry_count);
        } else {
            status = status_t::err_parsing_failed;
            LOG_E("len mismatch: %u", options.len);
            return status;
        }
        // read keys
        //LOG_D("\nRead Keys: \n");
        buffer.clear();
        //LOG_I("status:%s, entry_count:%d", status.as_string().c_str(), options.entry_count);
        std::size_t key_section_size = ((key_size * 2) * (options.entry_count / 2));
        for (std::size_t keys_count = 0; status.is_okay() && keys_count < options.entry_count;
             ++keys_count) {
            //LOG_I("keys_index:%d", keys_count);
            hash_t hash = 0;
            offset_t position = 0;
            //LOG_I("remaining_data:%d, key_size:%d", buffer.remaining_data(), key_size);
            if (buffer.remaining_data() < key_size) {
                // TODO:: add max_attempts
                buffer.clear();
                while (status.is_okay() && buffer.len() < max_buff_len) {
                    status = storage.read(buffer);
                }
                if (status.is_error()) {
                    LOG_E("unable to fill buffer");
                    return status;
                }
            }
            //LOG_I("remaining_data:%d, key_size:%d", buffer.remaining_data(), key_size);
            if (buffer.get_type(hash).is_error()) {
                status = status_t::err_parsing_failed;
                LOG_E("unable to parse hash");
                return status;
            }
            //LOG_D("entry.hash:%d, pos:%d, %02x", hash, buffer.pos(), hash);
            if (buffer.get_type(position).is_error()) {
                status = status_t::err_parsing_failed;
                LOG_E("unable to parse position");
                return status;
            }
            //LOG_D("entry.position:%d, pos:%d, %02x", position, buffer.pos(), position);
            std::size_t key_index = (position - (options_size + key_section_size)) / value_size;
            map[hash] = key_index;
            //LOG_D("entry key_index:%d", key_index);
            Entry &entry = entries[key_index];
            entry.hash = hash;
            entry.position = position;
            if (buffer.get_type(entry.key.len).is_error()) {
                status = status_t::err_parsing_failed;
                LOG_E("unable to parse key.len");
                return status;
            }
            //LOG_D("entry.key.len:%d, pos:%d, %02x", entry.key.len, buffer.pos(), entry.key.len);
            if (buffer.get(entry.key.len, entry.key.value, sizeof(entry.key.value), 0).is_error()) {
                status = status_t::err_parsing_failed;
                LOG_E("unable to parse key.value");
                return status;
            }
            //LOG_D("entry.key.value(len):%d, pos:%d", entry.key.len, buffer.pos());
            buffer.pos(key_size);
        }
        // read values
        //LOG_D("\nRead Values: \n");
        buffer.clear();
        for (std::size_t values_index = 0; status.is_okay() && values_index < options.entry_count;
             ++values_index) {
            Entry &entry = entries[values_index];
            if (buffer.remaining_data() < value_size) {
                // TODO:: add max_attempts
                buffer.clear();
                while (status.is_okay() && buffer.len() < max_buff_len) {
                    status = storage.read(buffer);
                }
                if (status.is_error()) {
                    LOG_E("unable to fill buffer");
                    return status;
                }
            }
            if (buffer.get_type(entry.value.type).is_error()) {
                status = status_t::err_parsing_failed;
                LOG_E("unable to parse value.type");
                return status;
            }
            //LOG_D("entry.value.type:%d, pos:%d, %s", entry.value.type, buffer.pos(), types::get_type_label(static_cast<types::arg_t>(entry.value.type)).c_str());
            if (buffer.get_type(entry.value.bin.len).is_error()) {
                status = status_t::err_parsing_failed;
                LOG_E("unable to parse value.bin.len");
                return status;
            }
            //LOG_D("entry.value.bin.len:%d, pos:%d, %02x", entry.value.bin.len, buffer.pos(), entry.value.bin.len);
            if (buffer
                  .get(entry.value.bin.len, entry.value.bin.value, sizeof(entry.value.bin.value), 0)
                  .is_error()) {
                status = status_t::err_parsing_failed;
                LOG_E("unable to parse value.bin.value");
                return status;
            }
            //LOG_D("entry.value.bin.value(len):%d, pos:%d", entry.value.bin.len, buffer.pos());
            //log_as_hex(entry.value.bin.value, entry.value.bin.len);
            buffer.clear();
            //LOG_D("values_index: %u", values_index);
            //print_entry(entry);
        }
        storage.close();
        return status;
    }

    //erase storage
    Status clear()
    {
        Status status{ status_t::success, status_labels };
        status = storage.init();
        if(status.is_okay())
        {
            status = storage.clear();
        }
        storage.close();
        return status;
    }

    Status set(const char *p_key,
               const std::size_t key_len,
               const uint8_t *p_value,
               const std::size_t value_len,
               const util::types::arg_t type)
    {
        Status status{ status_t::success, status_labels };
        if (!p_key) {
            status = err_key_is_null;
            return status;
        }
        if (!p_value) {
            status = err_value_is_null;
            return status;
        }
        if (key_len > key_str_len) {
            status = err_key_max_len_exceeded;
            return status;
        }
        if (value_len > value_bin_len) {
            status = err_value_max_len_exceeded;
            return status;
        }

        hash_t hash = hash_fn(const_cast<char *>(p_key), key_len);
        if (!map.contains(hash)) {
            Entry &entry = entries[options.entry_count];
            entry.key.len = key_len;
            std::copy_n(p_key, key_len, entry.key.value);
            entry.value.bin.len = value_len;
            std::copy_n(p_value, value_len, entry.value.bin.value);
            entry.hash = hash;
            entry.value.type = type;
            map.insert(std::pair{hash, options.entry_count});
            ++options.entry_count;
            //print_entry(entry);
            return status;
        } else {
            std::size_t index = map[hash];
            Entry &entry = entries[index];
            entry.value.bin.len = value_len;
            std::copy_n(p_value, value_len, entry.value.bin.value);
            entry.value.type = type;
            //print_entry(entry);
            return status;
        }
    }

    Status get(const char *p_key,
               const std::size_t key_len,
               uint8_t *p_value,
               std::size_t value_len,
               util::types::arg_t type)
    {
        Status status{ status_t::success, status_labels };
        if (!p_key) {
            status = err_key_is_null;
            return status;
        }
        if (key_len > key_str_len) {
            LOG_E("key_len %zu > %zu ", key_len, key_str_len);
            status = err_key_max_len_exceeded;
            return status;
        }
        if (!p_value) {
            status = err_value_is_null;
            return status;
        }
        const hash_t hash = hash_fn(const_cast<char *>(p_key), key_len);
        return get(hash, p_value, value_len, type);
    }

    Status get(const hash_t hash, uint8_t *p_value, std::size_t value_len, util::types::arg_t type)
    {
        Status status{ status_t::success, status_labels };
        if (!p_value) {
            status = err_value_is_null;
            return status;
        }
        if (!map.contains(hash)) {
#if 0// logging
            LOG_E("map count: %u", map.size());
            for (const auto &[hash, entry_index] : map) {
                LOG_E("found hash:%x, index:%u", hash, entry_index);
            }
#endif
            status = err_key_is_missing;
            return status;
        }
        Entry &entry = entries[map[hash]];
        if (type != entry.value.type) {
            status = err_type_mismatch;
            return status;
        }
        if (value_len < util::types::get_type_size(type)) {
            LOG_E("value_len %zu > %zu ", value_len, util::types::get_type_size(type));
            status = err_value_max_len_exceeded;
            return status;
        }
        std::copy_n(entry.value.bin.value, entry.value.bin.len, p_value);
        return status;
    }
};  // class Keystore

}  // namespace keystore
}  // namespace util
#endif  // NCL_UTIL_KEYSTORE
