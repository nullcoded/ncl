#include "ncl/buffers/ArraySlice.h"
#include "ncl/logging/logging.h"
#include "ncl/util/types.h"
#include "ncl/util/util.h"
#include "ncl/util/misc.h"
#include <cassert>

void util::print_array(uint8_t *a, size_t len)
{
    const std::size_t buf_len = util::kb * 1;
    std::size_t pos = 0;
    char buf[buf_len ] = {};
    pos = util::print_as_hex(buf, sizeof(buf), a, len);
    assert(buf_len > pos);
    printf("%s\n", buf);
}

bool util::compare_array(uint8_t *a1, size_t len1, uint8_t *a2, size_t len2)
{
    if (len1 != len2) {
        LOG_E("len: %zu::%zu", len1, len2);
        return false;
    }
    for (uint i = 0; i < len1; ++i) {
        if (a1[i] != a2[i]) {
            LOG_E("idx:%u, %02x::%02x", i, a1[i], a2[i]);
            return false;
        }
    }
    return true;
}
