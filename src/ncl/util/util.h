#ifndef UTIL_UTIL_H
#define UTIL_UTIL_H

#include <cstdint>
#include <stdint.h>
#include <cstddef>
#ifdef PICO
#include <sys/endian.h>
#else
#include <endian.h>
#endif //PICO
#include <optional>
#include <time.h>
#include "ncl/util/types.h"
/**
 * util.h - miscillaneous utility funtions
 */

namespace util {
using time_fn_t = uint64_t (*)(void);
const uint64_t ns_per_ms = 1000000;
const uint64_t ns_per_us = 1000;
const uint64_t us_per_ms = 1000;
const uint64_t ms_per_s = 1000;
const uint64_t us_per_s = 1000000;

const uint32_t s_per_hour = (60*60);
const uint32_t s_per_day = (s_per_hour*24);

const size_t kb = 1024;
const size_t mb = (1024 * 1024);
const size_t gb = (1024 * 1024 * 1024);

struct Version {
    uint8_t major;
    uint8_t minor;
    uint8_t patch;
    uint8_t build;
}; // struct Version

size_t print_version(char *buf, const size_t buf_size, Version *p_version);

/**
 * get_uint_from_bytes - return a uintX_t from an array of bytes.
 */
template <typename T> static std::optional<T> get_uint_from_bytes(const std::uint8_t * const bytes, const size_t len)
{
    return (sizeof(T) > len) ? std::nullopt
                             : std::optional<T>{ *(reinterpret_cast<const T *>(&bytes[0])) };
}

/**
 * htobe - host to big endian, works on integers
 */
template <typename T> inline T htobe(T raw)
{
    static_assert(sizeof(T) == 1, "attempted to use htobe<T> for an unsupported type!");
    return 0;
}

template <> inline uint16_t htobe<uint16_t>(uint16_t raw)
{
    return htobe16(raw);
}
template <> inline uint32_t htobe<uint32_t>(uint32_t raw)
{
    return htobe32(raw);
}
template <> inline uint64_t htobe<uint64_t>(uint64_t raw)
{
    return htobe64(raw);
}
template <> inline int16_t htobe<int16_t>(int16_t raw)
{
    return htobe16(raw);
}
template <> inline int32_t htobe<int32_t>(int32_t raw)
{
    return htobe32(raw);
}
template <> inline int64_t htobe<int64_t>(int64_t raw)
{
    return htobe64(raw);
}

/**
 * print_as_hex - print to a given buffer the given array of bytes as space delimited hex
 */
size_t print_as_hex(char *buf, const size_t buf_size, const std::uint8_t* const data, const size_t len);

/**
 * print_as_string - print given array of bytes as a string
 */
size_t print_as_string(char *buf, const size_t buf_size, const std::uint8_t* const data, const size_t len);

uint64_t get_time_us();

void init(time_fn_t p_fn);


}  // namespace util
#endif  // UTIL_UTIL_H
