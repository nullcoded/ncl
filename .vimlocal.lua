vim.keymap.set("n", "<F2>", "<cmd>:CMakeBuild -j32<CR>", { desc = "build" })
vim.keymap.set("n", "<F4>", "<cmd>:CMakeTest --rerun-failed --output-on-failure<CR>", { desc = "run test" })
