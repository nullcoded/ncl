#ifndef NCL_UTIL_CRC32
#define NCL_UTIL_CRC32

#include <cstddef>
#include <stdint.h>
#include <array>

#include "ncl/buffers/ArraySlice.h"

namespace util {

constexpr unsigned long reflect(unsigned long data, unsigned char n_bits) {
    unsigned long reflection{0};
    for (unsigned long outmask{1}, inmask{1u << (n_bits - 1)}; inmask; inmask >>= 1, outmask <<= 1) {
        if (data & inmask) {
            reflection |= outmask;
        }
    }
    return reflection;
}

template <typename T, T polynomial, T initial_remainder, T final_xor_value, bool relect_data, bool reflect_remainder>
class CrcCal
{
public:
    constexpr T calc(const uint8_t *message, int n_bytes) const;

private:
    static constexpr uint32_t width{ 8 * sizeof(T) };
    static constexpr uint32_t topbit{ 1u << (uint32_t)(width - 1) };
    static constexpr auto crc_table = [] {
        std::array<T, 256> tbl{};
        for (int dividend = 0; dividend < 256; ++dividend) {
            T remainder = dividend << (width - 8);
            for (uint8_t bit = 8; bit > 0; --bit) {
                if (remainder & topbit) {
                    remainder = (remainder << 1) ^ polynomial;
                } else {
                    remainder = (remainder << 1);
                }
            }
            tbl[dividend] = remainder;
        }
        return tbl;
    }();
};

using Crc32 = CrcCal<uint32_t, 0x4C11DB7u, 0xFFFFFFFFu, 0xFFFFFFFFu, true, true>;

template <typename T, T polynomial, T initial_remainder, T final_xor_value, bool relect_data, bool reflect_remainder>
constexpr T CrcCal<T, polynomial, initial_remainder, final_xor_value, relect_data, reflect_remainder>::calc(const uint8_t *message, int n_bytes) const
{
    T remainder{initial_remainder};
    if (relect_data) {
        for (int byte = 0; byte < n_bytes; ++byte) {
            unsigned char data = reflect(message[byte], 8) ^ (remainder >> (width - 8));
            remainder = crc_table[data] ^ (remainder << 8);
        }
    } else {
        for (int byte = 0; byte < n_bytes; ++byte) {
            unsigned char data = (message[byte]) ^ (remainder >> (width - 8));
            remainder = crc_table[data] ^ (remainder << 8);
        }
    }

    if (reflect_remainder) {
        return reflect(remainder, width) ^ final_xor_value;
    } else {
        return remainder ^ final_xor_value;
    }
}  

}  // namespace util

#endif  // NCL_UTIL_CRC32
