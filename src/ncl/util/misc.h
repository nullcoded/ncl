#ifndef UTIL_MISC_H
#define UTIL_MISC_H


namespace util {
using test_fn = bool (*)(void);

void print_array(uint8_t *a, size_t len);
bool compare_array(uint8_t *a1, size_t len1, uint8_t *a2, size_t len2);

} // namespace util

#endif // UTIL_MISC_H
