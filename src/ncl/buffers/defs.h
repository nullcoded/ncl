#ifndef NULLCODED_BUFFERS_DEFS_H
#define NULLCODED_BUFFERS_DEFS_H

#include <map>
#include <string>
#include "ncl/util/Status.h"

namespace buffers {
enum status_t {
    success = 0,
    err_generic_fail,
    err_capacity_exceeded,
    err_length_exceeded,
    err_nullptr_not_allowed,
    err_empty_input,
    err_empty_output,
    err_output_capacity_exceeded,
};
const std::map<status_t, std::string> status_labels = {
    { success, "Buffer success" },
    { err_generic_fail, "Buffer generic error" },
    { err_capacity_exceeded, "Buffer capacity exceeded error" },
    { err_length_exceeded, "Buffer length exceeded error" },
    { err_nullptr_not_allowed, "Buffer received nullptr where not allowed" }, 
    { err_empty_input, "Buffer received an empty input where not allowed" },
    { err_empty_output, "Buffer received an empty output where not allowed" },
    { err_output_capacity_exceeded, "Buffer receied an output with capacity too small for request" },
};

using Status = util::Status<status_t>;

} //namespace buffers
#endif                                          // NULLCODED_BUFFERS_DEFS_H
