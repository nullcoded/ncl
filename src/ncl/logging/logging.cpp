#include "ncl/logging/logging.h"
#ifdef PICO
//#include "pico/stdlib.h" 
#endif //PICO

static logging::log_level_t g_verbosity = logging::log_level_t::silence;


bool logging::should_log(log_level_t log_level)
{
		return true;
}

void logging::set_verbosity(log_level_t verbosity)
{
    g_verbosity = verbosity;
}

void logging::init(log_level_t verbosity)
{
    set_verbosity(verbosity);
#ifdef PICO
    //stdio_init_all();
#endif //PICO
}
