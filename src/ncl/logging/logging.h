#ifndef LOGGING_H
#define LOGGING_H
#include <stdint.h>
#include <stdio.h>
#include <iostream>
#include <cassert>

#include "ncl/util/util.h"

#ifndef PRINT_TIME
#define PRINT_TIME false
#endif

#if PRINT_TIME
#define LOG_(log_level, ...) \
    do { \
        if (should_log(log_level)) { \
            log(/*util::get_time_us()*/ 0, log_level, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__); \
        } \
    } while (false)
#else
#if 1
#define LOG_(log_level, ...) \
    do { \
        if (should_log(log_level)) { \
            log(0, log_level, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__); \
        } \
    } while (false)
#   else

#   define LOG_(log_level, ...)
#   endif
#endif 

#define LOG(...)   LOG_(logging::log_level_t::always, __VA_ARGS__)
#define LOG_V(...) LOG_(logging::log_level_t::verbose, __VA_ARGS__)
#define LOG_I(...) LOG_(logging::log_level_t::info, __VA_ARGS__)
#define LOG_D(...) LOG_(logging::log_level_t::debug, __VA_ARGS__)
#define LOG_E(...) LOG_(logging::log_level_t::error, __VA_ARGS__)

namespace logging {

enum log_level_t {
    always=-127,
    silence=0,
    verbose,
    info,
    debug,
    error,
};

bool should_log(log_level_t log_level);
void set_verbosity(log_level_t verbosity);
void init(log_level_t verbosity);

template <typename... Args> void log(uint64_t time,
                                     log_level_t log_level,
                                     const char* const file,
                                     size_t line_num,
                                     const char* const func,
                                     Args... args)
{
    char c = ' ';
    if (should_log(log_level)) {
        switch (log_level) {
        case logging::log_level_t::verbose:
            c = 'V';
            break;
        case logging::log_level_t::info:
            c = 'I';
            break;
        case logging::log_level_t::debug:
            c = 'D';
            break;
        case logging::log_level_t::error:
            c = 'E';
            break;
        default:
            break;
        }
    }
    const size_t buf_size = 512;
    char buf[buf_size];
#if PRINT_TIME
    size_t pos = sprintf(buf, "%09.05f:%c:%zu:%s:%s::", time / ((double)(util::us_per_s)), c, line_num, file, func);
#else
    size_t pos = sprintf(buf, "%c:%zu:%s:%s::", c, line_num, file, func);
#endif
    pos += sprintf(buf + pos, args...);
    buf[pos] = '\n';
    ++pos;
    buf[pos] = '\0';
    assert((pos < buf_size));
    printf(buf);
}

}  // namespace logging
#endif  // LOGGING_H
