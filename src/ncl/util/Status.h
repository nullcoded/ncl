#ifndef NULLCODED_UTIL_STATUS_H
#define NULLCODED_UTIL_STATUS_H
#include <cerrno>
#include <cstdint>
#include <cstring>
#include <map>
#include <string>
#include <utility>

#include "ncl/logging/logging.h"

namespace util {
class StatusInterface {
   public:
    virtual uint32_t as_u32() const 
    {
        return 0;
    };
    virtual std::string as_string() const
    {
        return "";
    };
    virtual void print() const
    {
        LOG("state:%s", as_string().c_str());
    }
    virtual void print_error()
    {
        if (errno) {
            LOG_E(" 1- %s, errno: %hhu, %s", as_string().c_str(), errno, strerror(errno));
        } else {
            LOG_E("2- %s", as_string().c_str());
        }
    }
    virtual bool is_success() const
    {
        return false;
    };
    virtual bool is_okay() const
    {
        return false;
    };
    virtual bool is_error() const
    {
        return false;
    };
};
/*
 *	Status - takes an enum and a enum->string mapping. represents a status
 *code and a human readable message.
 * The enums must have a success status and any errors must be greater than
 *success status, any non error status must be less than success status i.e.:
 *{okay1, okay2, success, error1, error2}
 */
template <typename status_t>
class Status : public StatusInterface {
    struct Members {
        // current status
        uint16_t category;
        status_t status;
        // level of verbosity controls what messages get printed
        logging::log_level_t verbosity;
        status_t success_status;
    } m;

    // all entries less than eSuccessState are Okay status, any more than
    // eSuccessState are Error status
    const std::map<status_t, std::string> *labels;

    inline Status(Members &&m_) : m(m_)
    {
    }

   public:
    Status(const status_t success, const std::map<status_t, std::string> &mapstr)
        : m(Members{ .status = success,
                     .verbosity = logging::log_level_t::error,
                     .success_status = success }),
          labels(&mapstr)
    {
    }
    Status(const status_t status,
           const status_t success,
           const logging::log_level_t verbosity,
           const std::map<status_t, std::string> &mapstr)
        : m(Members{ .status = status, .verbosity = verbosity, .success_status = success }),
          labels(&mapstr)
    {
    }
    Status(Status &other) : m(other.m), labels(other.labels)
    {
    }
    Status(Status &&other)
        : m(std::exchange(other.m, Members{})), labels(other.labels)
    {
    }
    uint32_t as_u32() const
    {
        return static_cast<uint32_t>(m.status);
    }
    std::string as_string() const
    {
        return labels->at(m.status);
    }
    void set(const status_t new_status)
    {
        if (new_status != m.status) {
            m.status = new_status;
            //LOG_E("%s", as_string().c_str());
            if (is_error() && m.verbosity != logging::log_level_t::silence) {
                print_error();
            } else if (m.verbosity >= logging::log_level_t::info) {
                LOG_I("%s", as_string().c_str());
            }
        }
    }

    status_t get() const
    {
        return m.status;
    }
    Status &operator=(status_t new_status)
    {
        set(new_status);
        return *this;
    }
    Status &operator=(Status *pNewStatus)
    {
        set(pNewStatus->get());
        return *this;
    }
    Status &operator=(Status newStatus)
    {
        set(newStatus.get());
        return *this;
    }
    bool operator==(const status_t new_status) const
    {
        return get() == new_status;
    }
    bool operator==(const Status &new_status) const
    {
        return (get() == new_status.get());
    }
    bool is_success() const
    {
        return m.status == m.success_status;
    }
    bool is_okay() const
    {
        return m.status <= m.success_status;
    }
    bool is_error() const 
    {
        return m.status > m.success_status;
    }
    void enable_logging(bool enable)
    {
        m.verbosity = enable ? logging::log_level_t::info : logging::log_level_t::silence;
    }
    void enable_error_logging(bool enable)
    {
        m.verbosity = enable ? logging::log_level_t::error : logging::log_level_t::silence;
    }

   public:
    ssize_t error_code = 0;
    ssize_t context = 0;
};

};      // namespace util
#endif  // NULLCODED_UTIL_STATUS_H
