#ifndef UTIL_TYPES_H
#define UTIL_TYPES_H

#include <cstdint>
#include <map>
#include <string>

namespace util {
namespace types {

using time_us_t = uint64_t;
using uint_t = unsigned int;
using int_t = int;

enum arg_t {
    nil,
    u8,
    u16,
    u32,
    u64,
    i8,
    i16,
    i32,
    i64,
    f32,
    f64,
    u8a,
    u16a,
    u32a,
    u64a,
    i8a,
    i16a,
    i32a,
    i64a,
    f32a,
    f64a,
    utf8,
    type_count,
    type_not_found
};  // enum bin_type_t

const std::map<arg_t, std::string> arg_labels = {
    { arg_t::nil, "nil" },
    { arg_t::u8, "u8" },
    { arg_t::u16, "u16" },
    { arg_t::u32, "u32" },
    { arg_t::u64, "u64" },
    { arg_t::i8, "i8" },
    { arg_t::i16, "i16" },
    { arg_t::i32, "i32" },
    { arg_t::i64, "i64" },
    { arg_t::f32, "f32" },
    { arg_t::f64, "f64" },
    { arg_t::u8a, "u8a" },
    { arg_t::u16a, "u16a" },
    { arg_t::u32a, "u32a" },
    { arg_t::u64a, "u64a" },
    { arg_t::i8a, "i8a" },
    { arg_t::i16a, "i16a" },
    { arg_t::i32a, "i32a" },
    { arg_t::i64a, "i64a" },
    { arg_t::f32a, "f32a" },
    { arg_t::f64a, "f64a" },
    { arg_t::utf8, "utf8" },
    { arg_t::type_not_found, "error: type_not_found" },
};
inline std::size_t get_type_size(arg_t t)
{
    switch (t) {
    case u8:
    case i8:
    case u8a:
    case i8a:
    case utf8:
        return sizeof(uint8_t);
    case u16:
    case i16:
    case u16a:
    case i16a:
        return sizeof(uint16_t);
    case u32:
    case i32:
    case u32a:
    case i32a:
        return sizeof(uint32_t);
    case u64:
    case i64:
    case u64a:
    case i64a:
        return sizeof(uint64_t);
    case f32:
    case f32a:
        return sizeof(float);
    case f64:
    case f64a:
        return sizeof(double);
    default:
        return type_not_found;
    }
}

inline bool is_valid_type(arg_t t)
{
    return t < type_count && t > nil;
}

inline std::string get_type_label(arg_t t)
{
    if (is_valid_type(t)) {
        return arg_labels.at(t);
    }
    return arg_labels.at(type_not_found);
}

}  // namespace types
}  // namespace util
#endif  // UTIL_TYPES_H
