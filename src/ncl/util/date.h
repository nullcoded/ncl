#ifndef NCL_UTIL_DATE_H
#define NCL_UTIL_DATE_H

#include <cstdint>
#include "ncl/util/types.h"
#include "ncl/util/util.h"
namespace util {
namespace date {

    enum weekdays {
        sunday,
        monday,
        tuesday,
        wednesday,
        thursday,
        friday,
        saturday
    };

    const uint32_t weekday_offset = 4;
    const uint8_t days_per_week = 7;
    const utin32_t next_week = util::s_per_day*days_per_week;

    std::uint8_t get_day_of_week(std::uint64_t now)
    {
        return ((now/util::s_per_day)%days_per_week))+weekday_offset;
    }

    std::uint8_t get_hour_of_day(std::uint64_t now)
    {
        return (now%util::s_per_day)/util::s_per_hour;
    }
} //namespace date
} //namespace util

#endif //define NCL_UTIL_DATE_H
