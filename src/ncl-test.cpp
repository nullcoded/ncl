#include <algorithm>
#include <assert.h>
#include <cstdint>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>

#include "ncl/buffers/ArraySlice.h"
#include "ncl/util/Crc32.h"
#include "ncl/util/Keystore.h"
#include "ncl/util/State.h"
#include "ncl/util/Status.h"

using test_fn = bool (*)(void);

void print_array(uint8_t *a, size_t len)
{
    char buf[1024 * 4] = {};
    util::print_as_hex(buf, sizeof(buf), a, len);
    printf("%s\n", buf);
}

bool compare_array(uint8_t *a1, size_t len1, uint8_t *a2, size_t len2)
{
    if (len1 != len2) {
        LOG_E("len mismatch: %zu, %zu", len1, len2);
        return false;
    }
    for (int i = 0; i < len1; ++i) {
        if (a1[i] != a2[i]) {
            LOG_E("i:%u %u!=%u", i, a1[i], a2[i]);
            return false;
        }
    }
    return true;
}
bool compare_array(char *a1, size_t len1, uint8_t *a2, size_t len2)
{
    return compare_array(reinterpret_cast<uint8_t *>(a1), len1, a2, len2);
}

bool compare_array(buffers::Array &buffer, uint8_t *a2, size_t len2)
{
    size_t len1 = buffer.len();
    return compare_array(buffer.data(), len1, a2, len2);
}

enum status_t {
    ok = -127,
    success = 0,
    fail,
};
enum state_t {
    init,
    start,
    stop,
};

const std::map<status_t, std::string> status_labels
  = { { fail, "fail" }, { success, "success" }, { ok, "ok" } };
const std::map<state_t, std::string> state_labels
  = { { init, "init" }, { start, "start" }, { stop, "stop" } };

uint64_t fn_get_time_us(void)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    uint64_t time = ts.tv_sec;
    time = time + (ts.tv_nsec / util::ns_per_us);
    return time;
}

bool test_keystore()
{
    class FileStorage : public util::keystore::StorageHandler {
        const int invalid_fd = -1;
        struct Members {
            int fd;
            char path[256];
            size_t read_count;
            size_t write_count;
        } m;

       public:
        FileStorage(const char *path) : m(Members{ .fd = invalid_fd })
        {
            std::size_t len = strlen(path);
            assert(len + 1 <= sizeof(m.path));
            std::copy_n(path, len, m.path);
        }
        virtual util::keystore::Status init()
        {
            util::keystore::Status status(util::keystore::status_t::success,
                                          util::keystore::status_labels);
            m.fd = open(m.path, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
            if (-1 == m.fd) {
                status = util::keystore::status_t::err_storage_init_failed;
            }
            return status;
        }
        virtual util::keystore::Status write(uint8_t *p, std::size_t len)
        {
            util::keystore::Status status(util::keystore::status_t::success,
                                          util::keystore::status_labels);
            size_t write_count = ::write(m.fd, p, len);
            if (-1 == write_count) {
                status = util::keystore::status_t::err_storage_write_failed;
            }
            if (write_count > 0) {
                m.write_count += write_count;
                LOG_D("write: %d", write_count);
                LOG_D("write_count: %d", m.write_count);
            }
            return status;
        }
        virtual util::keystore::Status read(uint8_t *buffer,
                                            std::size_t capacity,
                                            ssize_t &read_count)
        {
            util::keystore::Status status(util::keystore::status_t::success,
                                          util::keystore::status_labels);
            errno = 0;
            read_count = ::read(m.fd, buffer, capacity);
            if (-1 == read_count) {
                status = util::keystore::status_t::err_storage_read_failed;
            }
            if (read_count > 0) {
                m.read_count += read_count;
                LOG_D("read_count: %d", m.read_count);
            }
            if (0 == read_count && errno != 0) {
                LOG_D("read_count: %d, errno: %d, errstr: %s", read_count, errno, strerror(errno));
            }
            return status;
        }
        virtual util::keystore::Status close()
        {
            util::keystore::Status status(util::keystore::status_t::success,
                                          util::keystore::status_labels);
            if (m.fd >= 0) {
                ::close(m.fd);
            }
            m.fd = invalid_fd;
            return status;
        }
        virtual ~FileStorage()
        {
            close();
        }
    };  // class FileStorage
    bool is_pass = true;
    util::keystore::Crc32Hash hasher{};
    FileStorage storage{ "./test-keystore.nks" };
    const size_t storage_size = (1024 * 1024) / 8;
    util::keystore::Keystore<storage_size> ks{ util::keystore::hash_fn_t(hasher), &storage };
    const char key1[] = "one";
    uint32_t val1 = 1;
    if (ks.set(key1,
               strlen(key1),
               reinterpret_cast<uint8_t *>(&val1),
               sizeof(val1),
               util::types::arg_t::u32)
          .is_error()) {
        LOG_E("failed to set %s:%d", key1, val1);
        return false;
    }
    const char key2[] = "two";
    float val2 = 2.0;
    if (ks.set(key2,
               strlen(key2),
               reinterpret_cast<uint8_t *>(&val2),
               sizeof(val2),
               util::types::arg_t::f32)
          .is_error()) {
        LOG_E("failed to set %s:%d", key2, val2);
        return false;
    }
    if (ks.serialize().is_error()) {
        LOG_E("%s", "failed to serialize");
        return false;
    }
    //
    util::keystore::Keystore<storage_size> ks2{ util::keystore::hash_fn_t(hasher), &storage };
    ks2.deserialize();
    util::Crc32 crc_gen{};
    util::keystore::hash_t hash1_b
      = crc_gen.calc(reinterpret_cast<const uint8_t *>(key1), strlen(key1));
    uint32_t val1_b = 0;
    std::size_t val1_len = sizeof(val1_b);
    ks2.get(
      key1, strlen(key1), reinterpret_cast<uint8_t *>(&val1_b), val1_len, util::types::arg_t::u32);
    if (val1 != val1_b) {
        LOG_E("failed to get val1: %lu != %lu", val1, val1_b);
        return false;
    }
    util::keystore::hash_t hash_key2
      = crc_gen.calc(reinterpret_cast<const uint8_t *>(key2), strlen(key2));
    float val2_b = 0;
    std::size_t val2_len = sizeof(val2_b);
    ks2.get(hash_key2, reinterpret_cast<uint8_t *>(&val2_b), val2_len, util::types::arg_t::f32);
    if (val2 != val2_b) {
        LOG_E("failed to get val2: %f != %f", val2, val2_b);
        print_array(reinterpret_cast<uint8_t *>(&val2), sizeof(val2));
        print_array(reinterpret_cast<uint8_t *>(&val2_b), sizeof(val2_b));
        return false;
    }
    return is_pass;
}

bool test_array_fill()
{
    LOG_I("%s", "test_array_fill");
    uint8_t expected[] = { 0x20, 0x20, 0x20, 0x20, 0x20 };
    const size_t expected_len = sizeof(expected);
    util::Status status(buffers::status_t::success, buffers::status_labels);
    buffers::ArrayBuffer buffer(10);
    status = buffer.fill(0x20, 5);
    if (!compare_array(buffer, expected, expected_len)) {
        LOG_I("fill failed, buffer did not match expected len:%u", buffer.len());
        buffer.print_as_hex();
        print_array(expected, expected_len);
        return false;
    }
    return status.is_success();
}

bool test_status()
{
    util::Status<status_t> status(success, status_labels);
    status.enable_logging(true);
    status = ok;
    if (status.is_success() || status.is_error() || !status.is_okay()) {
        LOG_D("status in unexpected state: %u-%s", status.as_u32(), status.as_string().c_str());
        return false;
    }
    status = fail;
    if (status.is_success() || status.is_okay() || !status.is_error()) {
        LOG_D("status in unexpected state: %u-%s", status.as_u32(), status.as_string().c_str());
        return false;
    }
    status = success;
    if (status.is_error() || !status.is_okay() || !status.is_success()) {
        LOG_D("status in unexpected state: %u-%s, is_error:%d, is_okay:%d, is_success:%d",
              status.as_u32(),
              status.as_string().c_str(),
              status.is_error(),
              status.is_okay(),
              status.is_success());
        return false;
    }
    return true;
}
bool test_state()
{
    util::State<state_t> state(state_t::init, state_labels);
    state.enable_logging(true);
    state = start;
    if (state.get() != start) {
        LOG_D("state in unexpected state: %u", state.get());
        return false;
    }
    sleep(1);
    state = stop;
    if (state.get() != stop) {
        LOG_D("state in unexpected state: %u", state.get());
        return false;
    }
    return true;
}

bool test_buffer()
{
    buffers::ArrayBuffer buffer(10);
    uint8_t a[] = { 0, 1, 2, 3, 4 };
    uint8_t b[] = { 0, 0, 0 };
    if (buffer.len() != 0 || buffer.capacity() != 10) {
        buffer.print_as_hex();
        LOG_I("len:%u", buffer.len());
        LOG_E("invalid initial ArrayBuffer: len: %u, cap: %u", buffer.len(), buffer.capacity());
        return false;
    }
    buffer.push(a, sizeof(a)).print();
    if (!compare_array(buffer, a, sizeof(a))) {
        LOG_I("len:%u", buffer.len());
        LOG_E("%s", "push failed, buffer doesnt match!");
        print_array(a, sizeof(a));
        buffer.print_as_hex();
        return false;
    }
    buffer.push(a, sizeof(a)).print();
    uint8_t a2[] = { 0, 1, 2, 3, 4, 0, 1, 2, 3, 4 };
    if (!compare_array(buffer, a2, sizeof(a2))) {
        LOG_E("%s", "push failed, buffer doesnt match!");
        LOG_I("len:%u", buffer.len());
        buffer.print_as_hex();
        print_array(a2, sizeof(a2));
        return false;
    }

    buffer.pop(sizeof(b), b, sizeof(b), 0).print();
    if (!compare_array(b, sizeof(b), a, sizeof(b))) {
        LOG_D("%s", "pop failed: returned data doesnt match expected");
        print_array(b, sizeof(b));
        print_array(a, sizeof(b));
        return false;
    }
    if (buffer.len() != 7) {
        LOG_I("len:%u", buffer.len());
        LOG_E("pop failed: buffer left in inconsistent state! %d != %d", buffer.len(), 7);
        buffer.print_as_hex();
        return false;
    }
    LOG_D("%s", "done");
    return true;
}

bool test_crc()
{
    uint8_t prot[] = "senack";
    util::Crc32 crc_gen{};
    uint32_t crc32 = 0;
    crc32 = crc_gen.calc(prot, 6);
    uint32_t expected = 0x6bfa8762;
    if (crc32 != expected) {
        LOG_D("crc didnt match expectd %x!=%x", crc32, expected);
        return false;
    }
    return true;
}

int main(int argc, char *argv[])
{
    util::init(fn_get_time_us);
    std::pair<std::string, test_fn> tests[] = {
        { "test_buffer", test_buffer },         { "test_state", test_state },
        { "test_status", test_status },         { "test_crc", test_crc },
        { "test_array_fill", test_array_fill }, { "test_keystore", test_keystore },
    };
    bool all_success = true;
    for (std::pair<std::string, test_fn> pair : tests) {
        bool test_success = pair.second();
        printf("test:%s::%s\n\n", pair.first.c_str(), test_success ? "PASS" : "FAIL");
        all_success = all_success && test_success;
    }
    return all_success ? 0 : -1;
}
